#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "tabwidget.h"


class MainWindow : public QWidget
{
    Q_OBJECT

    TabWidget*  _tabs;

public:
    explicit MainWindow(QWidget *parent = 0);

signals:

public slots:

};

#endif // MAINWINDOW_H
