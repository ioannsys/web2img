#ifndef WEBBROWSER_H
#define WEBBROWSER_H


#include <QWidget>
#include <QDate>
#include <QTime>
#include <QTimer>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QTimeLine>
#include <QPushButton>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QTimeEdit>

#include <QMessageBox>
#include <QProgressBar>
#include <QPixmap>

#include <QApplication>
#include <QWebEngineView>
#include <QWebEngineSettings>
#include <QDesktopWidget>

#include <QThread>
#include <QDir>

#include <iostream>
#include <string>

// internal resources
// icons
extern const char* data_icon_reload;
extern const char* data_icon_grab;

class WebBrowser : public QWidget
{
    Q_OBJECT

    void applyLayout();

    QLineEdit*      _editURL;
    QTimeEdit*      _editTimer;
    QPushButton*    _btnReload;
    QPushButton*    _btnStartPause;
    QGroupBox*      _groupFormats;
    QCheckBox*      _chboxPNG;
    QCheckBox*      _chboxJPG;
    QCheckBox*      _chboxPDF;
    QWebEngineView* _webView;
    QTimer*         _timer;
    QProgressBar*   _timeProgress;

    QPixmap         _img;

    bool grabState;


public:
    explicit WebBrowser(QWidget *parent = 0);

signals:
    void PageLoaded(const QString& title, bool statusOk);

public slots:


private slots:
    void HandleButton_Reload(void);
    void HandleURL_textChanged(const QString& text);
    void HandleButton_StartPause(void);
    void TakeScreenshot();

};

#endif // WEBBROWSER_H
