#include "tabwidget.h"

TabWidget::TabWidget(QWidget *parent) : QTabWidget(parent)
{
    QWebEngineSettings::globalSettings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
    QWebEngineSettings::globalSettings()->setAttribute(QWebEngineSettings::ScreenCaptureEnabled, true);

    setTabsClosable(true);
    QVBoxLayout *generalLay = new QVBoxLayout;
    _empty = new QWidget(this);
    generalLay->addWidget(new QLabel("<center><p><font size = 16 font color = '#aa3232'> Press \"<b>+</b>\" tab for creating new instance.</p></center>", _empty));
    _empty->setLayout(generalLay);

    addTab(_empty, "+");
    tabBar()->tabButton(0, QTabBar::RightSide)->hide();
    QShortcut *shortcut_NewTab = new QShortcut(QKeySequence("CTRL+N"), parent);

    connect(this, &TabWidget::tabBarClicked, this, &TabWidget::AddNewTarget);
    connect(this, &TabWidget::tabCloseRequested, this, &TabWidget::RemoveTarget);

    connect(shortcut_NewTab, &QShortcut::activated, [=]()
    {
        emit tabBarClicked( indexOf(_empty) );
    });

}



void TabWidget::AddNewTarget(int index)
{
    if (index > 24 )
    {
        QMessageBox::warning(this, "Tabs limit!", "Can not create more than 24 tabs.");
        return;
    }
    if(tabText(index) != "+")
    {
        return;
    }

    WebBrowser* webPage = new WebBrowser(this);
    QString tabName = "New Target ";
    insertTab(index, webPage, tabName);
    setCurrentWidget(webPage);

    QShortcut* shortcut_CloseTab = new QShortcut(QKeySequence("CTRL+W"), webPage);
    connect(shortcut_CloseTab, &QShortcut::activated, [=]() { RemoveTarget( indexOf(webPage)); });

    connect(webPage, &WebBrowser::PageLoaded, [=](const QString& title, bool status)
    {
        status ? setTabText(indexOf(webPage), title.mid(0, 32)) : setTabText(indexOf(webPage) , tabName + QString("%1").arg(index + 1) + " [ERROR - page not found!!!]");
    });

}




void TabWidget::RemoveTarget(int index)
{
    int result = QMessageBox::warning(nullptr, "Delete target?", "Remove this target?",
                                      QMessageBox::Button::Yes | QMessageBox::Button::No,
                                      QMessageBox::Button::No);


    if(result == QMessageBox::Yes)
    {
        WebBrowser* webWidget = static_cast<WebBrowser*> (widget(index));
        removeTab(index);
        webWidget->close();
    }
}
