#include "webbrowser.h"
WebBrowser::WebBrowser(QWidget *parent)
    : QWidget(parent),
      grabState(false),
      _img(QPixmap(65535, 65535))
{
    // setParent(parent);
    setAttribute(Qt::WA_DeleteOnClose, true);

    // controls
    _timer = new QTimer(this);
    _timer->setSingleShot(false);
    _timer->setTimerType(Qt::CoarseTimer);

    _editURL = new QLineEdit(this);
    _editURL->setPlaceholderText("URL address: (http://example.com)");
    _editURL->setToolTip("Enter the URL-address here.");

    _btnReload = new QPushButton(QPixmap::fromImage(QImage::fromData(QByteArray::fromBase64(data_icon_reload))), "", this);
    _btnReload->setToolTip("Reload page");

    _editTimer = new QTimeEdit(QTime(0, 0, 3), this);
    _editTimer->setDisplayFormat("H:mm:ss");
    _editTimer->setToolTip("Timer interval in H:mm:ss resolution for grabbing web-content.");
    _editTimer->setCurrentSection(QTimeEdit::MinuteSection);

    _chboxJPG = new QCheckBox("JPG", this);
    _chboxJPG->setChecked(true);
    _chboxPNG = new QCheckBox("PNG", this);
    _chboxPDF = new QCheckBox("PDF", this);

    _btnStartPause = new QPushButton(QPixmap::fromImage(QImage::fromData(QByteArray::fromBase64(data_icon_grab))),"START", this);
    _btnStartPause->setToolTip("Start/Stop process grabbing.");
    _btnStartPause->setDisabled(true);

    _webView = new QWebEngineView(this);

    _timeProgress = new QProgressBar(this);
    _timeProgress->setStyleSheet("text-align: center;");

    // layout organization
    QHBoxLayout *layURL     = new QHBoxLayout;
    QVBoxLayout *layGeneral = new QVBoxLayout;
    layURL->addWidget(_editURL, 2);
    layURL->addWidget(_btnReload);
    layURL->addWidget(_editTimer);
    layURL->addWidget(_chboxJPG);
    layURL->addWidget(_chboxPNG);
    layURL->addWidget(_chboxPDF);
    layURL->addWidget(_timeProgress);
    layURL->addWidget(_btnStartPause);

    layGeneral->addLayout(layURL);
    layGeneral->addWidget(_webView);


    setLayout(layGeneral);

    /*
        connections/bindings
    */
    /// This timer can start after pageLoad status
    //connect(_timer, &QTimer::timeout, [=]()     {   TakeScreenshot();   });
    connect( _timer, &QTimer::timeout, this, &WebBrowser::TakeScreenshot);

    connect(_editURL, &QLineEdit::textChanged, this, &WebBrowser::HandleURL_textChanged);
    connect(_editURL, &QLineEdit::returnPressed, [=]()      {   _webView->load(QUrl::fromUserInput(_editURL->text()));});
    connect(_btnReload, &QPushButton::clicked, _webView, &QWebEngineView::reload);
    connect(_btnStartPause, &QPushButton::clicked, this, &WebBrowser::HandleButton_StartPause);

    connect(_webView, &QWebEngineView::loadProgress, [=](int progress)
    {
        _timeProgress->setValue(progress);
        _editURL->setFocus();
    });

    connect(_webView, &QWebEngineView::loadFinished, [=](bool ok)
    {
        if (ok)
        {
                _webView->url().toString() == "about:blank" ? ok = false : _editURL->setText(_webView->url().toString());
        }

        _btnStartPause->setEnabled(ok);
        emit PageLoaded(_webView->title(), ok);
    } );

    connect(_webView->page(), &QWebEnginePage::renderProcessTerminated, this, [=](QWebEnginePage::RenderProcessTerminationStatus state, int code){
        if (state)
            std::cout << "Abnormal rendering termination. Code = " <<  code << std::endl;
        ;});
}

void WebBrowser::TakeScreenshot()
{
    /* TODO Inactive behaviour

    Qt::ApplicationState state = qApp->applicationState();
    if( state == Qt::ApplicationInactive)
    {
        std::cout << "This application INACTIVE!" << std::endl;
        _webView->reload();
        this->thread()->msleep(500);
    }
    */

    QWidget *tmpWindow = new QWidget;
    tmpWindow->setAttribute(Qt::WA_DontShowOnScreen, true);
    tmpWindow->setAttribute(Qt::WA_ShowWithoutActivating, true);
    tmpWindow->setAttribute(Qt::WA_DeleteOnClose, true);
    tmpWindow->setAttribute(Qt::WA_AlwaysStackOnTop, true);

    tmpWindow->resize(size());
    tmpWindow->setLayout(layout());
    tmpWindow->show();

    QDateTime stamp = QDateTime::currentDateTime();
    QString outFileName = QString("OUTPUT/%1_%2_%3.")
            .arg(stamp.date().toString("yyyy-MM-dd"))
            .arg(stamp.time().toString("HHmmss"))
            .arg(_webView->url().host());

    _img = _webView->grab();
   if( _chboxPNG->isChecked() )
    {
       _img.save(outFileName + "png");
    }

   if( _chboxJPG->isChecked() )
   {
       _img.save(outFileName + "jpg");
   }

   if( _chboxPDF->isChecked() )
   {
       _webView->page()->printToPdf(outFileName + "pdf", QPageLayout(QPageSize(QPageSize::A4), QPageLayout::Landscape, QMarginsF()));
   }

    setLayout(tmpWindow->layout());
    tmpWindow->close();

}



void WebBrowser::HandleURL_textChanged(const QString& text)
{
    _webView->setUrl(QUrl::fromUserInput(text));
    _editURL->setFocus();
}



void WebBrowser::HandleButton_Reload()
{
    _webView->reload();
    parentWidget()->update();
}



void WebBrowser::HandleButton_StartPause()
{
    grabState = !grabState;
    if( grabState )
    {
        _btnStartPause->setText("STOP");
        _editTimer->setDisabled(true);

       const QTime tm = _editTimer->time();
        /*
        int ss = tm.second() * 1000;
        int mm = tm.minute() * 60 * 1000;
        int hh = tm.hour() * 60 * 60 * 1000;
        int interval = ss + mm + hh;

        std::cout << "Native ms = " << QTime(0, 0, 0).msecsTo(tm) << std::endl;
        */

        _timer->start( (QTime(0, 0, 0).msecsTo(tm)) );
        // std::cout << "Timer time left: " << _timer->remainingTime() << "ms\n" << std::endl;
    }
    else
    {
        _btnStartPause->setText("START");
        _editTimer->setDisabled(false);
        _timer->stop();
    }
}

// icons
const char* data_icon_reload = {
    "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyY"
    "XRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVk"
    "cgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzM"
    "ub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGlu"
    "ayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iM"
    "CAwIDYwIDYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA2MCA2MDsiIHhtbD"
    "pzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjMycHgiPgo8Zz4KCTxwYXR"
    "oIGQ9Ik0zNC45MjUsMjBsLTguNTEzLDYuMTkxYy0wLjQ0NywwLjMyNC0wLjU0NiwwLjk1LTAu"
    "MjIxLDEuMzk2YzAuMTk1LDAuMjcsMC41LDAuNDEyLDAuODEsMC40MTIgICBjMC4yMDQsMCwwL"
    "jQwOS0wLjA2MywwLjU4Ny0wLjE5MUwzOS43LDE5bC0xMi4xMTItOC44MDljLTAuNDQ4LTAuMz"
    "I1LTEuMDczLTAuMjI3LTEuMzk2LDAuMjIxICAgYy0wLjMyNSwwLjQ0Ni0wLjIyNywxLjA3Miw"
    "wLjIyMSwxLjM5NkwzNC45MjUsMThIMjNjLTcuMTY4LDAtMTMsNS44MzItMTMsMTNjMCwwLjU1"
    "MywwLjQ0NywxLDEsMXMxLTAuNDQ3LDEtMSAgIGMwLTYuMDY1LDQuOTM1LTExLDExLTExSDM0L"
    "jkyNXoiIGZpbGw9IiMwMDAwMDAiLz4KCTxwYXRoIGQ9Ik0zMCwwQzEzLjQ1OCwwLDAsMTMuND"
    "U4LDAsMzBzMTMuNDU4LDMwLDMwLDMwczMwLTEzLjQ1OCwzMC0zMFM0Ni41NDIsMCwzMCwweiB"
    "NMzAsNThDMTQuNTYxLDU4LDIsNDUuNDM5LDIsMzAgICBTMTQuNTYxLDIsMzAsMnMyOCwxMi41"
    "NjEsMjgsMjhTNDUuNDM5LDU4LDMwLDU4eiIgZmlsbD0iIzAwMDAwMCIvPgoJPHBhdGggZD0iT"
    "TQ4LDI4Yy0wLjU1MywwLTEsMC40NDctMSwxYzAsNi4wNjUtNC45MzUsMTEtMTEsMTFIMjQuMD"
    "c1bDguNTEzLTYuMTkxYzAuNDQ3LTAuMzI0LDAuNTQ2LTAuOTUsMC4yMjEtMS4zOTYgICBjLTA"
    "uMzI0LTAuNDQ3LTAuOTQ5LTAuNTQ2LTEuMzk2LTAuMjIxTDE5LjMsNDFsMTIuMTEyLDguODA5"
    "QzMxLjU5LDQ5LjkzOCwzMS43OTUsNTAsMzEuOTk5LDUwYzAuMzEsMCwwLjYxNC0wLjE0MywwL"
    "jgxLTAuNDEyICAgYzAuMzI1LTAuNDQ2LDAuMjI3LTEuMDcyLTAuMjIxLTEuMzk2TDI0LjA3NS"
    "w0MkgzNmM3LjE2OCwwLDEzLTUuODMyLDEzLTEzQzQ5LDI4LjQ0Nyw0OC41NTMsMjgsNDgsMjh"
    "6IiBmaWxsPSIjMDAwMDAwIi8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+"
    "CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+C"
    "jwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg=="
};
const char* data_icon_grab = {
    "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyY"
    "XRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVk"
    "cgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzM"
    "ub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGlu"
    "ayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iM"
    "CAwIDYwIDYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA2MCA2MDsiIHhtbD"
    "pzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+CjxnPgoJPHB"
    "hdGggZD0iTTU1LjIwMSwxNS41aC04LjUyNGwtNC0xMEgxNy4zMjNsLTQsMTBIMTJ2LTVINnY1"
    "SDQuNzk5QzIuMTUyLDE1LjUsMCwxNy42NTIsMCwyMC4yOTl2MjkuMzY4ICAgQzAsNTIuMzMyL"
    "DIuMTY4LDU0LjUsNC44MzMsNTQuNWg1MC4zMzRjMi42NjUsMCw0LjgzMy0yLjE2OCw0LjgzMy"
    "00LjgzM1YyMC4yOTlDNjAsMTcuNjUyLDU3Ljg0OCwxNS41LDU1LjIwMSwxNS41eiBNOCwxMi4"
    "1aDJ2M0g4ICAgVjEyLjV6IE01OCw0OS42NjdjMCwxLjU2My0xLjI3MSwyLjgzMy0yLjgzMywy"
    "LjgzM0g0LjgzM0MzLjI3MSw1Mi41LDIsNTEuMjI5LDIsNDkuNjY3VjIwLjI5OUMyLDE4Ljc1N"
    "iwzLjI1NiwxNy41LDQuNzk5LDE3LjVINmg2ICAgaDIuNjc3bDQtMTBoMjIuNjQ2bDQsMTBoOS"
    "44NzhjMS41NDMsMCwyLjc5OSwxLjI1NiwyLjc5OSwyLjc5OVY0OS42Njd6IiBmaWxsPSIjODI"
    "wNTFjIi8+Cgk8cGF0aCBkPSJNMzAsMTQuNWMtOS45MjUsMC0xOCw4LjA3NS0xOCwxOHM4LjA3"
    "NSwxOCwxOCwxOHMxOC04LjA3NSwxOC0xOFMzOS45MjUsMTQuNSwzMCwxNC41eiBNMzAsNDguN"
    "WMtOC44MjIsMC0xNi03LjE3OC0xNi0xNiAgIHM3LjE3OC0xNiwxNi0xNnMxNiw3LjE3OCwxNi"
    "wxNlMzOC44MjIsNDguNSwzMCw0OC41eiIgZmlsbD0iIzgyMDUxYyIvPgoJPHBhdGggZD0iTTM"
    "wLDIwLjVjLTYuNjE3LDAtMTIsNS4zODMtMTIsMTJzNS4zODMsMTIsMTIsMTJzMTItNS4zODMs"
    "MTItMTJTMzYuNjE3LDIwLjUsMzAsMjAuNXogTTMwLDQyLjVjLTUuNTE0LDAtMTAtNC40ODYtM"
    "TAtMTAgICBzNC40ODYtMTAsMTAtMTBzMTAsNC40ODYsMTAsMTBTMzUuNTE0LDQyLjUsMzAsND"
    "IuNXoiIGZpbGw9IiM4MjA1MWMiLz4KCTxwYXRoIGQ9Ik01MiwxOS41Yy0yLjIwNiwwLTQsMS4"
    "3OTQtNCw0czEuNzk0LDQsNCw0czQtMS43OTQsNC00UzU0LjIwNiwxOS41LDUyLDE5LjV6IE01"
    "MiwyNS41Yy0xLjEwMywwLTItMC44OTctMi0yczAuODk3LTIsMi0yICAgczIsMC44OTcsMiwyU"
    "zUzLjEwMywyNS41LDUyLDI1LjV6IiBmaWxsPSIjODIwNTFjIi8+CjwvZz4KPGc+CjwvZz4KPG"
    "c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc"
    "+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+"
    "CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg=="
};
