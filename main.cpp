#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);
    QCoreApplication::setAttribute(Qt::AA_DisableShaderDiskCache, true);
    // QCoreApplication::setAttribute(Qt::AA_ForceRasterWidgets, true);
    QCoreApplication::setAttribute(Qt::AA_NativeWindows, false);
    QCoreApplication::testAttribute(Qt::AA_UseDesktopOpenGL) ? QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL, true)
                                                             : QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL, true);

    QApplication a(argc, argv);

    MainWindow wnd;
    wnd.show();

    return a.exec();
}
