#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QTabWidget>
#include <QPushButton>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QTabBar>
#include <QShortcut>

#include "webbrowser.h"

class TabWidget : public QTabWidget
{
    Q_OBJECT

    // QPushButton*    _addTab;
    QWidget*    _empty;

public:
    TabWidget(QWidget* parent = 0);

public slots:
    void AddNewTarget(int index);
    void RemoveTarget(int index);
};

#endif // TABWIDGET_H
