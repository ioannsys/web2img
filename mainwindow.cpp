#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    // Check OUTPUT folder
    // FIXME: need test case for write
    if (!QDir("OUTPUT").exists())
    {
        QDir().mkdir("OUTPUT");
    }

    // Main window settings & attributes
    // setAttribute(Qt::WA_NativeWindow, false);

    setWindowTitle("Web to image - " + QDir::currentPath());
    resize(1024, 768);

    QVBoxLayout *layGeneral = new QVBoxLayout;

    _tabs = new TabWidget(this);
    layGeneral->addWidget(_tabs);

    setLayout(layGeneral);
}
